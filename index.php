<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>KnockoutJS Checklist</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    </head>
    <body>
        <div id="checkList" class="container wrapper checklist-mainblock">
            <header>
                <h1 class="text-success">KnockoutJS Checklist</h1>
            </header>
            <div class="form-inline new-task">
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Новое напоминание" data-bind="textInput: newTaskTitle">
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-outline-success add-task-button" data-bind="click: addTask">Добавить напоминание</button>
                </div>
            </div>
            <div class="row all-tasks">
                <div class="col-lg-6 bg-light">
                    <h4 class="text-center">Напоминания</h4>
                    <div data-bind="visible: tasks().length">
                        <ul class="tasks-list" data-bind="foreach: tasks">
                            <li class="task checkbox">
                                <label data-bind="click: $parent.checkTask">
                                    <input type="checkbox">
                                    <span data-bind="text: title"></span>
                                    <a class="remove-task" href="#" data-bind="click: $parent.removeTask">
                                        (удалить)
                                    </a>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="text-center mb-2" data-bind="visible: !tasks().length">
                        Нет напоминаний
                    </div>
                </div>
                <div class="col-lg-6 bg-success">
                    <h4 class="text-white text-center">Выполненные напоминания</h4>
                    <div data-bind="visible: completeTasks().length">
                        <ul class="tasks-list" data-bind="foreach: completeTasks">
                            <li class="task">
                                <span class="text-white" data-bind="text: title"></span>
                                <a class="undo-task text-danger" href="#" data-bind="click: $parent.undoTask">
                                    (отменить)
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="text-center text-white mb-2" data-bind="visible: !completeTasks().length">
                        Нет выполненных напоминаний
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/knockout-3.5.1.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
