(function (ko) {
    var CheckListViewModel = function () {
        var self = this;
        
        var checklist = new Checklist();
        
        this.checklist = checklist;
        this.newTaskTitle = ko.observable('');
        this.tasks = ko.observableArray();
        this.completeTasks = ko.observableArray();
        
        this.addTask = function () {
            this.checklist.addTask(this.newTaskTitle());
            this.newTaskTitle('');
            this.tasks(this.checklist.tasks);
        };
        
        this.removeTask = function (taskObject, event) {
            self.checklist.removeTask(taskObject.id);
            self.tasks(self.checklist.tasks);
        };

        this.checkTask = function (taskObject, event) {
            self.checklist.checkTask(taskObject.id);
            self.tasks(self.checklist.tasks);
            self.completeTasks(self.checklist.completeTasks);
        }; 
        
        this.undoTask = function (taskObject, event) {
            self.checklist.undoTask(taskObject.id);
            self.tasks(self.checklist.tasks);
            self.completeTasks(self.checklist.completeTasks);
        };
        
        $.getJSON("/loadtable.php", function(data){
            $.each( data, function( key, value ) {
                var vals = JSON.stringify(value);
                var parsed = JSON.parse(vals);
                parsed['title'] = parsed['name'];
                
                [0, 1, 2, 'name'].forEach(e => delete parsed[e]);
                
                if (parsed.is_complete != true) {
                    self.checklist.tasks.push(parsed);
                }else {
                    self.checklist.completeTasks.push(parsed);
                }

                self.tasks(self.checklist.tasks);
                self.completeTasks(self.checklist.completeTasks);

            });
        });
    };
    
    var Checklist = function () {
        this.tasks = [];
        this.completeTasks = [];
        this.addTask = function (taskTitle) {
  
            let item = {
                func : 'addTask',
                taskTitle : taskTitle
            };
            
            if (item.taskTitle.length) {
                $.ajax({
                    url:'functions.php',
                    dataType: 'json',
                    type: 'GET',
                    data: item
                }).done(function (data) {
                    if (data && data.errors && data.errors.length ) {
                        console.error(data);
                    } 
                });
            }
                
                this.tasks.push({
                    title:taskTitle 
                });
                
        };
        
        this.removeTask = function (id) {
            let item = {
                func : 'removeTask',
                id : id
            };
            
            if (item.id) {
                $.ajax({
                    url:'functions.php',
                    dataType: 'json',
                    type: 'GET',
                    data: item
                }).done(function (data) {
                    if (data && data.errors && data.errors.length ) {
                        console.error(data);
                    } 
                });
            }
            
            var taskIndex = this.getIndexById(id, this.tasks);
            
            if (typeof taskIndex !== 'undefined') {
                this.tasks.splice(taskIndex, 1);
            }
        };
        
        this.checkTask = function (id) {
            let item = {
                func : 'checkTask',
                id : id
            };

            if (item.id) {
                $.ajax({
                    url:'functions.php',
                    dataType: 'json',
                    type: 'GET',
                    data: item
                }).done(function (data) {
                    if (data && data.errors && data.errors.length ) {
                        console.error(data);
                    } 
                });
            }
            
            var taskIndex = this.getIndexById(id, this.tasks), task;
            
            if (typeof taskIndex !== 'undefined') {
                task = this.tasks[taskIndex];
                this.tasks.splice(taskIndex, 1);
                this.completeTasks.push(task);
            }   
        };
        
        this.undoTask = function (id) {
            
            let item = {
                func : 'undoTask',
                id : id
            };
            
            if (item.id) {
                $.ajax({
                    url:'functions.php',
                    dataType: 'json',
                    type: 'GET',
                    data: item
                }).done(function (data) {
                    if (data && data.errors && data.errors.length ) {
                        console.error(data);
                    } 
                });
            }
            
            var taskIndex = this.getIndexById(id, this.completeTasks), task;
            
            if (typeof taskIndex !== 'undefined') {
                task = this.completeTasks[taskIndex];
                this.completeTasks.splice(taskIndex, 1);
                this.tasks.push(task);
            } 
        };
        
        this.getIndexById = function (id, tasks) {
            var index;
            
            for (var i = 0, max = tasks.length; i < max; i++)
                if (tasks[i].id === id) {
                        index = i;
                        break;
                }
            return index;
        };
    };
  
    ko.applyBindings(new CheckListViewModel(), document.getElementById('checkList'))
})(ko);
