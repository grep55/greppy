<?php
require 'config/db_connection.php';

    function addTask($name){
        global $pdo;
        $sql = "INSERT INTO tasks (name) VALUES (:name)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->execute();
        return true;
    }
    
    function removeTask($id){
        global $pdo;
        $sql = "DELETE FROM tasks WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        return true;
    }
    
    function checkTask($id){
        global $pdo;
        $sql = "UPDATE tasks SET is_complete=true WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        return true;
    }
    
    function undoTask($id){
        global $pdo;
        $sql = "UPDATE tasks SET is_complete=false WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        return true;
    }
    
    if ($_REQUEST['func'] === 'addTask') {
        addTask($_REQUEST['taskTitle']);
    }elseif ($_REQUEST['func'] === 'removeTask') {
        removeTask($_REQUEST['id']);
    }elseif ($_REQUEST['func'] === 'checkTask') {
        checkTask($_REQUEST['id']);
    }elseif ($_REQUEST['func'] === 'undoTask') {
        undoTask($_REQUEST['id']);
    }
